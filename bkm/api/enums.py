from enum import Enum
import inspect


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        members = inspect.getmembers(cls, lambda m: not(inspect.isroutine(m)))
        props = [m for m in members if not(m[0][:2] == '__')]
        choices = tuple([(str(p[1].value), p[0]) for p in props])
        return choices


class Gender(ChoiceEnum):
    MALE = 'male'
    FEMALE = 'demale'
    OTHER = 'other'


class Role(ChoiceEnum):
    MENTOR = 'mentor'
    MENTEE = 'mentee'
    ADMIN = 'admin'


class WayToProgram(ChoiceEnum):
    FRIEND = 'from_your_friend'
    BKM_FANPAGE = 'from_BKM_fanpage'
    OTHER_BK_FANPAGES = 'from_other_BK_fanpages'
    CAREER_CENTER = 'from_career_centers'
    HANOI_ALUMNI_MENTORING = 'from_hanoi_alumni_mentoring'
    UEH_FANPAGE = 'from_UEH_fanpage'
    OTHER_UEH_FANPAGES = 'from_other_UEH_fanpages'


class QuestionType(ChoiceEnum):
    SELECT = "Trắc nghiệm"
    LEVEL_SELECT = "Chọn mức độ"
    MUTIPLE_SELECT = "Chọn nhiều đáp án"
    SORTABLE = "Sắp xếp thứ tự"
    WRITE_CONTENT = "Tự luận"
    FILE = 'Tệp'
    HIDDEN = 'Ẩn'


class ProfileStatus(ChoiceEnum):
    # Stage 1
    NOT_SUBMIT = 'not_submit'
    SUBMITTED = 'submitted'
    # Stage 2
    ADMIN_ACCEPT = 'admin_accept'
    ADMIN_REJECT = 'admin_reject'


class RequestStatus(ChoiceEnum):
    # Stage 3
    WAITING_MENTOR = 'waiting_mentor'
    MENTOR_ACCEPT = 'mentor_accept'
    MENTOR_REJECT = 'mentor_reject'
    INTERVIEWED = 'interviewed'


class MatchingStatus(ChoiceEnum):
    # Stage 3
    MATCHING_FALL = 'matching_fall'
    MATCHING_COMPLETED = 'matching_completed'
