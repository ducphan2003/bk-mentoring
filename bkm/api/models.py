import uuid
# import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from api.enums import *


class AbstractEntity(models.Model):
    id = models.CharField(max_length=255, primary_key=True,
                          default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(
        'created_at', default=timezone.now)
    updated_at = models.DateTimeField(
        'updated_at', default=timezone.now, blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(AbstractEntity, self).save(*args, **kwargs)


# Account manager
class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email, password=password, **extra_fields)
        user.is_admin = True
        user.save(using=self._db)
        return user


# User
class User(AbstractEntity, AbstractBaseUser):
    fullname = models.CharField(max_length=50)
    email = models.EmailField(max_length=100, unique=True)
    gender = models.CharField(choices=Gender.choices(),
                              default='male', max_length=100)
    date_of_birth = models.DateField()
    phone_number = models.CharField(max_length=20)
    way_to_program = models.CharField(
        choices=WayToProgram.choices(), default='male', max_length=225)
    facebook = models.CharField(max_length=100, blank=True)
    role = models.CharField(choices=Role.choices(),
                            default='mentee', max_length=50)
    avatar = models.FileField(upload_to='user_avatar', blank=True)

    # Add more field if user is mentee
    student_id = models.CharField(max_length=30, blank=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    REQUIRED_FIELDS = ['fullname', 'gender', 'date_of_birth',
                       'phone_number', 'way_to_program', 'role']

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        db_table = 'user'
        default_permissions = ()
        ordering = ['id']


# School Information
class School(AbstractEntity):
    name = models.CharField(max_length=100)
    user = models.ManyToManyField(User)

    class Meta:
        db_table = 'school'
        ordering = ['id']
        default_permissions = ()


# All possible questions on profile
class Question(AbstractEntity):
    content = models.CharField(max_length=1000)
    question_type = models.CharField(
        max_length=100, choices=QuestionType.choices(), default='SELECT')
    question_group = models.CharField(
        max_length=100, choices=Role.choices(), default='mentee')
    description = models.CharField(max_length=225, blank=True)
    is_required = models.BooleanField(default=False)

    class Meta:
        db_table = 'question'
        default_permissions = ()
        ordering = ['id']


# The answer to each question in the user's profile
class Answer(AbstractEntity):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'answer'
        default_permissions = ()
        ordering = ['id']


#  Answers in order form
class AnswerLevelSelect(AbstractEntity):
    level = models.SmallIntegerField()
    answer = models.OneToOneField(Answer, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'answer_level_select'
        default_permissions = ()
        ordering = ['id']


#  Answers in order form
class AnswerSortable(AbstractEntity):
    content = models.CharField(max_length=255)
    answer = models.OneToOneField(Answer, on_delete=models.CASCADE, null=True)
    self_order = models.IntegerField(default=1)
    user_order = models.IntegerField(default=1)

    class Meta:
        db_table = 'answer_sortable'
        default_permissions = ()
        ordering = ['id']


# Answers in the form of choosing answers
class AnswerSelect(AbstractEntity):
    content = models.CharField(max_length=255)
    answer = models.OneToOneField(Answer, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'answer_select'
        default_permissions = ()
        ordering = ['id']


# Answers in text form
class AnswerContent(AbstractEntity):
    content = models.CharField(max_length=1000)
    answer = models.OneToOneField(Answer, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'answer_content'
        default_permissions = ()
        ordering = ['id']


#  File upload answer
class AnswerFile(AbstractEntity):
    file_upload = models.FileField(upload_to='user_answer_file', null=True)
    answer = models.OneToOneField(Answer, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'answer_file'
        default_permissions = ()
        ordering = ['id']


# Save user profile information
class Profile(AbstractEntity):
    status = models.CharField(
        max_length=100, choices=ProfileStatus.choices(), default='not_submit')
    start_day = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, null=True)
    user = models.OneToOneField(
        User, null=True, on_delete=models.CASCADE)
    answer = models.ManyToManyField(Answer)

    class Meta:
        db_table = 'profile'
        default_permissions = ()
        ordering = ['id']


# Save rejected application information
class ProfileReject(AbstractEntity):
    reason = models.CharField(max_length=225)
    profile = models.OneToOneField(
        Profile, null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'profile_reject'
        default_permissions = ()
        ordering = ['id']


# Save request information
class Request(AbstractEntity):
    status = models.CharField(
        max_length=100, choices=RequestStatus.choices(), default='WAITING_MENTOR')
    mentee = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name='mentee_request')
    mentor = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name='mentor_request')

    class Meta:
        db_table = 'request'
        default_permissions = ()
        ordering = ['id']


# User matching requests denied
class RequestReject(AbstractEntity):
    request = models.OneToOneField(
        Request, on_delete=models.CASCADE, null=True)
    reason = models.CharField(max_length=1000)

    class Meta:
        db_table = 'request_reject'
        default_permissions = ()
        ordering = ['id']


# User matching requests accept
class RequestAccept(AbstractEntity):
    request = models.OneToOneField(
        Request, on_delete=models.CASCADE, null=True)
    time_meeting_1 = models.DateTimeField(auto_now=False, auto_now_add=False)
    time_meeting_2 = models.DateTimeField(auto_now=False, auto_now_add=False)
    time_meeting_3 = models.DateTimeField(auto_now=False, auto_now_add=False)
    time_meeting_chosen = models.DateTimeField(
        auto_now=False, auto_now_add=False, blank=True)

    class Meta:
        db_table = 'request_accept'
        default_permissions = ()
        ordering = ['id']


# Matching and interviewing information of mentee with mentor
class Matching(AbstractEntity):
    request_accept = models.OneToOneField(
        RequestAccept, on_delete=models.CASCADE, null=True)
    mentee_confirm = models.BooleanField(default=False)
    mentor_confirm = models.BooleanField(default=False)
    status = models.CharField(max_length=100, choices=MatchingStatus.choices())

    class Meta:
        db_table = 'matching'
        default_permissions = ()
        ordering = ['id']


# Mentee failed the interview
class MatchingFall(AbstractEntity):
    matching = models.OneToOneField(
        Matching, on_delete=models.CASCADE, null=True)
    reason = models.CharField(max_length=1000)

    class Meta:
        db_table = 'matching_fall'
        default_permissions = ()
        ordering = ['id']
