
import os
import csv
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.decorators import api_view

from django_filters import rest_framework as filters

from .models import Question, User, School
from .permissions import IsOwner, IsUserAdmin
from .serializers import UserSerializer, RegisterSerializer
from .paginations import CustomPagination
from .filters import UserFilter


class RegisterView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class ListCreateUserAPIView(ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, IsUserAdmin]
    pagination_class = CustomPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter


class RetrieveUpdateDestroyUserAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated, IsOwner]
    lookup_field = 'email'


fileDir = os.path.dirname(os.path.realpath('__file__'))


@api_view(['POST', 'GET'])
def importData(request):
    with open(os.path.join(fileDir, 'api\\test_data\\schools.csv'), encoding="utf8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        for row in csv_reader:
            School.objects.get_or_create(name=row[0])

    with open(os.path.join(fileDir, 'api\\test_data\\questions.csv'), encoding="utf8") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter='|')
        for row in csv_reader:
            Question.objects.get_or_create(
                content=row[0], question_group=row[1], description=row[2], is_required=row[3], question_type=row[4])

    return Response({"message": "Import data successful"})
