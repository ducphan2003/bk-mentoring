from django_filters import rest_framework as filters
from .models import User


class UserFilter(filters.FilterSet):
    fullname = filters.CharFilter(lookup_expr='icontains')
    phone_number = filters.CharFilter(lookup_expr='icontains')
    email = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = User
        fields = ['fullname', 'phone_number', 'email']