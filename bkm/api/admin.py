from django.contrib import admin

from api.models import (
    AnswerSelect, AnswerContent, AnswerFile, AnswerSortable,
    Answer, Profile, ProfileReject, User, School,
    RequestReject, Question, Request,
    Matching, MatchingFall, RequestAccept,
)

admin.site.register(AnswerSelect)
admin.site.register(AnswerContent)
admin.site.register(AnswerFile)
admin.site.register(AnswerSortable)
admin.site.register(Answer)
admin.site.register(Profile)
admin.site.register(ProfileReject)
admin.site.register(User)
admin.site.register(School)
admin.site.register(RequestReject)
admin.site.register(Question)
admin.site.register(Request)
admin.site.register(Matching)
admin.site.register(MatchingFall)
admin.site.register(RequestAccept)
