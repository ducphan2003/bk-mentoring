from django.urls import path
from api.views import (
    RegisterView, ListCreateUserAPIView, RetrieveUpdateDestroyUserAPIView,
)

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


from . import views


urlpatterns = [
    path('import-data/', views.importData),

    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='auth_register'),

    path('users/', ListCreateUserAPIView.as_view(), name='get_post_user'),
    path('users/<email>/', RetrieveUpdateDestroyUserAPIView.as_view(),
         name='get_delete_update_user'),
]
