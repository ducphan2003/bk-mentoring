from functools import partial
from rest_framework import serializers
from api.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserSerializer(RegisterSerializer):

    class Meta:
        model = User
        fields = '__all__'

    def update(self, instance, validated_data):
        if validated_data.get('password'):
            password = validated_data.pop('password')
            instance.set_password(password)
        instance.save()
        return super(UserSerializer, self).update(instance, validated_data)
