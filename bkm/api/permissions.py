from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied


class IsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.email == request.user.email


class IsUserAdmin(permissions.BasePermission):

    def has_permission(self, request, view):
        print(request.user.email)
        return request.user.is_admin

