# BK-Mentoring

## Getting started

```
pip install -r requirements.txt
cd bkm
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## APIs

| Endpoint            | HTTP Method | CRUD Method | Result                        | Role  |
| ------------------- | ----------- | ----------- | ----------------------------- | ----- |
| `api/register`      | POST        | CREATE      | Register new user account     |       |
| `api/token`         | POST        | CREATE      | Get login token               |       |
| `api/token/refresh` | POST        | CREATE      | Get login refresh token       |       |
| `api/users`         | GET         | READ        | Get all user account          | admin |
| `api/users/:email/` | GET         | READ        | Get a single user account     | owner |
| `api/users`         | POST        | CREATE      | Create a new user account     | admin |
| `api/users/:email/` | PUT         | UPDATE      | Update a user account         | owner |
| `api/users/:email/` | PATCH       | UPDATE      | Partial update a user account | owner |
| `api/users/:email/` | DELETE      | DELETE      | Delete a user account         | owner |
| `api/import-data/`  | GET, POST   | CREATE      | Create test data              |       |
